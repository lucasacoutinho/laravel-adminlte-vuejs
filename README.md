
# LARAVEL ADMINLTE VUEJS

Integração do AdminLTE e do VueJs com o laravel.


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/lucasacoutinho/laravel-basicos.git
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies

```bash
  composer install
```

```bash
  cp .env.example .env
```

```bash
  php artisan key:generate
```

```bash
  php artisan migrate
```

```bash
  php artisan db:seed
```

```bash
  npm install
```

Start the server

```bash
  npm run dev
```

```bash
  php artisan serve
```

## FAQ

#### Materiais Utilizados

• **[Integrate Laravel 8 with AdminLTE 3 and Font Awesome 5 | AdminLTE 3 con Laravel 8 y Font Awesome 5](https://www.youtube.com/watch?v=Krc-4MIHFlk)**

• **[Ambientando Vue no Laravel](https://www.youtube.com/watch?v=NtM9B12lARw)**
