<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') }} | @yield('title')</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div id="app" class="wrapper">
        @include('partials.navbar')
        
        <section class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </section>
          
        <footer class="footer mt-auto py-3 bg-dark">
            <div class="container">
                <strong>Copyright &copy; 2021-{{now()->format('Y')}} <a href="{{route('home')}}">{{env('APP_NAME')}}</a>.</strong> All rights reserved.
            </div>
        </footer>
    </div>

    <script src="{{asset('js/app.js')}}"></script>
</body>

</html>