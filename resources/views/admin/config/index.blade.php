@extends('layouts.admin')
@section('title', 'Configurações')
@section('subtitle', 'Configurações')
@section('caption', '')

@section('content')
<div>
  <div class="bg-white p-2 rounded">
    <div class="row">
      <div class="col">
        <h5 class="font-weight-bold">Nome da aplicação</h5>
        <div class="text-lg">{{env('APP_NAME')}}</div>
      </div>
      <div class="col">
        <h5 class="font-weight-bold">Nome do criador</h5>
        <div class="text-lg">{{env('APP_NAME')}}</div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <h5 class="font-weight-bold">Email da aplicação</h5>
        <div class="text-lg">{{env('APP_NAME')}}</div>
      </div>
      <div class="col">
        <h5 class="font-weight-bold">Logo da aplicação</h5>
        <div class="text-lg">{{env('APP_NAME')}}</div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <h5 class="font-weight-bold">Data de criação da aplicação</h5>
        <div class="text-lg">{{env('APP_NAME')}}</div>
      </div>
    </div>
  </div>
</div>
@endsection