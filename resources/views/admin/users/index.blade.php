@extends('layouts.admin')
@section('title', 'Usuarios')
@section('subtitle', 'Usuarios')
@section('caption', '')

@section('content')
    <users-component :users="{{$users}}"/>
@endsection
