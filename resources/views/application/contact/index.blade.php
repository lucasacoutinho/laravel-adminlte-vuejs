@extends('layouts.main')
@section('title', 'Contato')

@section('content')
<div class="container h-full">
    <section>
        <div class="my-4">
            <h1 class="display-4 text-center">Contato</h1>
            <p class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                the leap into electronic typesetting, remaining essentially unchanged. <span class="text-success">It was
                    popularised in the 1960s</span> with the release of Letraset sheets containing Lorem Ipsum passages,
                and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem
                Ipsum.</p>
        </div>
    </section>

    <section>
        <div class="d-flex align-items-center justify-content-center">
            @for ($i = 0; $i < 3; $i++) <div class="text-center my-4  mx-3 bg-white w-25 rounded py-3 px-2">
                <h4>Telefone</h4>
                <p>+55038988787831</p>
        </div>
        @endfor
</div>
</section>
<section>
    <div class="my-4">
        <h1 class="display-4 text-center">Envie uma mensagem</h1>
    </div>
        <div class="d-flex rounded justify-content-center align-content-center">
            <img src="{{asset('img/contact/imagem-1-500x500.jpg')}}" class="rounded-left">
            <contact-form-component/>
        </div>
</section>
</div>
@endsection