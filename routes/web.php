<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Application\{HomeController, PriceController, ContactController};
use App\Http\Controllers\Admin\Config\ConfigController;
use App\Http\Controllers\Admin\Usuarios\UsuarioController;

Route::prefix('admin')->name('admin.')->group(function () {
    Route::view('/', 'admin.home.index')->name('index');
    Route::get('/usuarios', [UsuarioController::class, 'index'])->name('users.index');
    Route::get('/configuracoes', [ConfigController::class, 'index'])->name('config.index');
});

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/precos', [PriceController::class, 'index'])->name('price');
Route::get('/contato', [ContactController::class, 'index'])->name('contact');