<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurationTable extends Migration
{
    public function up()
    {
        Schema::create('configuration', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('logo');
            $table->string('email');
            $table->string('criador');
            $table->timestamp('data_de_criacao');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('configuration');
    }
}
