<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index() {
        return view('application.home.index');
    }
}
