<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index() {
        return view('application.contact.index');
    }
}
