<?php

namespace App\Http\Controllers\Admin\Config;

use App\Http\Controllers\Controller;
use App\Models\Config;

class ConfigController extends Controller
{
    public function index() {
        return view('admin.config.index', ['config' => Config::first()]);
    }
}
