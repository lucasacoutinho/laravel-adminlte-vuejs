<?php

namespace App\Http\Controllers\Admin\Usuarios;

use App\Http\Controllers\Controller;
use App\Models\User;

class UsuarioController extends Controller
{
    public function index() {
        $usuarios = User::all();
        return view('admin.users.index', ['users' => $usuarios]);
    }
}
