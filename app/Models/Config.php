<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use HasFactory;

    protected $table = 'configuration';

    protected $fillable = ['nome', 'logo', 'email', 'criador', 'data_de_criacao'];
}
